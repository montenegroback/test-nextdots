<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Task;

class TasksController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index()
    {
        $tasks = Task::all();
        return response()->json($tasks);
    }

    public function store(Request $request){
        

        $task = Task::create($request->all());
        
        if($task){
            return response()->json($task);
        }
        
        return response()->json(["No se ha podido guardar la tarea"]);
    }

    public function show($id)
    {
        
        $task = Task::find($id);

        if($task)
        {
            return response()->json($task);
        }

        return response()->json(["Tarea no encontrada"]);

    }

    public function update(Request $request, $id)
    {
        
        $task                  = Task::find($id);
        $task->title           = $request->input('title');
        $task->description     = $request->input('description');
        $task->due_date        = $request->input('due_date');
        
        if($task->save())
        {
            return response()->json($task);
        }
        
        return response()->json(['No se ha podido actualizar la tarea']);
    }

    public function delete($id)
    {
        
        $task  = Task::find($id);
        
        if($task->delete())
        {
             return response()->json(['Tarea eliminada correctamente']);
        }
        
        return response()->json(['No se ha podido eliminar la tarea']);
          
       
    }

}
