<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index()
    {
        $users = User::all();
        return response()->json($users);
    }

    public function store(Request $request){
        

        $user = User::create($request->all());
        
        if($user){
            return response()->json($user);
        }
        
        return response()->json(["No se ha podido guardar el usuario"]);
    }

    public function show($id)
    {
        
        $user = User::find($id);

        if($user)
        {
            return response()->json($user);
        }

        return response()->json(["Usuario no encontrado"]);

    }

    public function update(Request $request, $id)
    {
        
        $user = User::find($id);
        $user->first_name   = $request->input('first_name');
        $user->last_name    = $request->input('model');
        $user->email        = $request->input('email');
        $user->password     = $request->input('password');
        
        if($user->save())
        {
            return response()->json($user);
        }
        
        return response()->json(['No se ha podido actualizar el usuario']);
    }

    public function delete($id)
    {
        
        $user  = User::find($id);
        
        if($user->delete())
        {
             return response()->json(['Usuario eliminado correctamente']);
        }
        
        return response()->json(['No se ha podido eliminar el usuario']);
          
       
    }

}
