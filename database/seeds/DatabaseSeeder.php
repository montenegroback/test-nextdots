<?php

use Illuminate\Database\Seeder;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       	$this->call('UsersTableSeeder');
    }
}


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
    	$new_user = new User();
    	$new_user->first_name = 'Administrador';
    	$new_user->last_name  = 'Nexdots';
    	$new_user->email      = 'admin@nexdots.com';
    	$new_user->password   = 'root';
    	$new_user->save();
    }
}